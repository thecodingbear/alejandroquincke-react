import React, {Component} from 'react';
import PrismicReact from 'prismic-reactjs';
import getData from '../../services/getData';
import './style.css';
import $ from 'jquery';

class Banner extends Component{

    constructor(){
        super();
        this.state = {
            titlePage : null
        }
    }

    componentWillMount() {

        getData('my.homepage.uid', 'homepage').then(result => {
            this.setState({
                titlePage : result[0].data.title["0"].text
            })
        }).catch(err => {
            console.log(err);
        })
    }

    componentDidMount() {
        // after render
        // this.jqueryFunctions();
    }

    jqueryFunctions(){

        var banner = $('#banner');
        var bannerHeight = banner.height();

        $(window).on('scroll', function(){

            let scrollPosition = $(window).scrollTop();

            let updateHeight = bannerHeight - (scrollPosition) * 3;
            if(updateHeight > 200){
                banner.css('height', updateHeight + 'px');
            }

        })

    }

    render(){
        return(
            <div id="banner">
                <div className="overlay white">
                    <div className="center">
                        <h1>{this.state.titlePage}</h1>
                        <img src="/images/logotipo_white.png" />
                    </div>
                </div>
            </div>
        );
    }

}

export default Banner;
